package org.example.actuator.controller;

import com.yonyou.cloud.actuate.configurate.EnableHealthCheck;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
// @Import({EndpointRegistryConfigurate.class, HealthEndpointConfiguration.class})
public class HealthController {
    @RequestMapping("/")
    public String index() {
        return "hello";
    }

    @RequestMapping("/ping")
    public String echo() {
        return "pong";
    }
}