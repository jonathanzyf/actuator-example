package org.example.actuator.config;

import com.yonyou.cloud.actuate.configurate.EnableHealthCheck;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

//@Configuration
@EnableHealthCheck
public class MyConfig  {
//    ApplicationContext applicationContext;
//    Environment environment;
//
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        this.applicationContext = applicationContext;
//    }
//
//    @Override
//    public void setEnvironment(Environment environment) {
//        this.environment = environment;
//    }

//    private Map<String, HealthIndicator> getAllHealthIndicator() {
//        String[] names = this.applicationContext.getBeanNamesForType(HealthIndicator.class);
//        Map<String, HealthIndicator> healthIndicators = Maps.newConcurrentMap();
//
//        for (int i = 0; i < names.length; ++i) {
//            Object obj = this.applicationContext.getBean(names[i]);
//            if (obj != null) {
//                healthIndicators.put(names[i], (HealthIndicator) obj);
//            }
//        }
//
//        return healthIndicators;
//    }
//
//    @Bean("healthEndpointYonyou")
//    public HealthEndpoint healthEndpoint() {
//        String cacheRefreshInterval = this.environment.getProperty("spring.healthcheck.interval", "30");
//        return new HealthEndpoint(Long.parseLong(cacheRefreshInterval), this.getAllHealthIndicator());
//    }
}
