# health check 在 spring mvc项目中问题
## 直接使用注解不可用
```java
@EnableHealthCheck
@RestController
public class HealthController {
    // ...
}
```

### 报错信息
```
HTTP ERROR 500 org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'healthEndpointYonyou' available
URI:	/health-check
STATUS:	500
MESSAGE:	org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'healthEndpointYonyou' available
SERVLET:	healthCheckServlet
CAUSED BY:	org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'healthEndpointYonyou' available
Caused by:
org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'healthEndpointYonyou' available
	at org.springframework.beans.factory.support.DefaultListableBeanFactory.getBeanDefinition(DefaultListableBeanFactory.java:816)
	at org.springframework.beans.factory.support.AbstractBeanFactory.getMergedLocalBeanDefinition(AbstractBeanFactory.java:1288)
	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:298)
	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:202)
	at org.springframework.context.support.AbstractApplicationContext.getBean(AbstractApplicationContext.java:1109)
	at com.yonyou.cloud.actuate.servlet.CheckServlet.handle(CheckServlet.java:78)
	at com.yonyou.cloud.actuate.servlet.CheckServlet.doGet(CheckServlet.java:49)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:687)
	at javax.servlet.http.HttpServlet.service(HttpServlet.java:790)
```
### 解决方法
1. 创建配置类
```java
@EnableHealthCheck
public class MyConfig  {
}
```
2. 在applicaiton-context.xml中声明
```xml
<bean id="myConfig" class="org.example.actuator.config.MyConfig" />
```
注意：不是spring-mvc.xml

### 原因分析
根据测试结果分析，应该是在父ApplicationContext中实例化HealthEndpoint才可以，在子ApplicationContext中不行。

## 启动时大量警告
```
[WARNING] com.yonyou.cloud.actuate.internal.com.google.common.util.concurrent.Service$State$2 scanned from multiple locations: jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-autoconfigurate/0.0.1-RELEASE/health-autoconfigurate-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$2.class, jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-core/0.0.1-RELEASE/health-core-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$2.class
[WARNING] com.yonyou.cloud.actuate.internal.com.google.common.util.concurrent.Service$State$3 scanned from multiple locations: jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-autoconfigurate/0.0.1-RELEASE/health-autoconfigurate-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$3.class, jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-core/0.0.1-RELEASE/health-core-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$3.class
[WARNING] com.yonyou.cloud.actuate.internal.com.google.common.util.concurrent.Service$State$4 scanned from multiple locations: jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-autoconfigurate/0.0.1-RELEASE/health-autoconfigurate-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$4.class, jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-core/0.0.1-RELEASE/health-core-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$4.class
[WARNING] com.yonyou.cloud.actuate.internal.com.google.common.util.concurrent.Service$State$5 scanned from multiple locations: jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-autoconfigurate/0.0.1-RELEASE/health-autoconfigurate-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$5.class, jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-core/0.0.1-RELEASE/health-core-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$5.class
[WARNING] com.yonyou.cloud.actuate.internal.com.google.common.util.concurrent.Service$State$6 scanned from multiple locations: jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-autoconfigurate/0.0.1-RELEASE/health-autoconfigurate-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$6.class, jar:file:///Users/yanfengzhao/.m2/repository/com/yonyou/cloud/actuator/health-core/0.0.1-RELEASE/health-core-0.0.1-RELEASE.jar!/com/yonyou/cloud/actuate/internal/com/google/common/util/concurrent/Service$State$6.class
```
这个需要解决。