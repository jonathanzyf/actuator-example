package org.example.iuap.actuator;

import com.yonyou.cloud.actuate.configurate.EnableHealthCheck;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableHealthCheck
@SpringBootApplication
public class UapActuatorApplication {
    public static void main(String[] args) {
        SpringApplication.run(UapActuatorApplication.class, args);
    }
}